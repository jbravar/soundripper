# soundripper

A tool to rip sounds

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/soundripper.svg)](https://npmjs.org/package/soundripper)
[![Downloads/week](https://img.shields.io/npm/dw/soundripper.svg)](https://npmjs.org/package/soundripper)
[![License](https://img.shields.io/npm/l/soundripper.svg)](https://github.com/jbravar/soundripper/blob/master/package.json)

<!-- toc -->
* [soundripper](#soundripper)
<!-- tocstop -->

## Usage

<!-- usage -->
```sh-session
$ npm install -g soundripper
$ soundripper COMMAND
running command...
$ soundripper (-v|--version|version)
soundripper/0.1.0 linux-x64 node-v12.7.0
$ soundripper --help [COMMAND]
USAGE
  $ soundripper COMMAND
...
```
<!-- usagestop -->

## Commands

<!-- commands -->
* [`soundripper help [COMMAND]`](#soundripper-help-command)
* [`soundripper rip URL`](#soundripper-rip-url)

## `soundripper help [COMMAND]`

display help for soundripper

```
USAGE
  $ soundripper help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_

## `soundripper rip URL`

rip a url

```
USAGE
  $ soundripper rip URL

ARGUMENTS
  URL  url to rip

EXAMPLE
  $ soundripper rip https://soundcloud.com/user/track
```
<!-- commandsstop -->
