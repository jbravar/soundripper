import axios from "axios";
import execa from "execa";
import getStream from "get-stream";
import { JSDOM } from "jsdom";
import { Command } from "@oclif/command";
import { stringify } from "querystring";

import { SCTrackInfo } from "../util/SCTrackInfo";
import { captureGroup } from "../util/text";

const USER_AGENT =
  "Mozilla/5.0 (X11; Linux i686; rv:10.0) Gecko/20100101 Firefox/10.0";

const instance = axios.create({
  headers: {
    "User-Agent": USER_AGENT
  }
});

async function findClientId(chunkUrls: string[]): Promise<string | null> {
  for (const chunkUrl of chunkUrls) {
    const { data: chunkString } = await instance.get<string>(chunkUrl);
    const found = chunkString.match(/client_id:"(?<id>\w+)"/mu);
    if (found && found.groups && found.groups.id) return found.groups.id;
  }

  return null;
}

export default class Rip extends Command {
  static description = "rip a url";
  static examples = [`$ soundripper rip https://soundcloud.com/user/track`];
  static args = [{ name: "url", required: true, description: "url to rip" }];

  async run() {
    const { args } = this.parse(Rip);

    try {
      const initialResponse = await instance.get(args.url);
      const pageDom = new JSDOM(initialResponse.data);
      const scriptChunks = [
        ...pageDom.window.document.querySelectorAll<HTMLScriptElement>(
          "script[src]"
        )
      ].map(script => script.src);

      /* eslint-disable @typescript-eslint/camelcase */
      const client_id = await findClientId(scriptChunks);
      if (!client_id) throw new Error("Cannot find client_id. Aborting.");

      const scriptObjects = [
        ...pageDom.window.document.querySelectorAll("script")
      ]
        .map(el => el.textContent)
        .filter(str => str && str.length)
        .map(str => str && captureGroup('"data":', str))
        .flatMap(strings =>
          strings ? strings.map(str => JSON.parse(str)) : []
        );

      const trackInfo: SCTrackInfo = scriptObjects
        .flat()
        .reduce((acc, val) => ({ ...acc, ...val }), {});

      const playlistUrlRequest = await instance({
        url: `${trackInfo.media.transcodings[0].url}?${stringify({
          client_id
        })}`,
        responseType: "stream"
      });
      /* eslint-enable @typescript-eslint/camelcase */

      const { url: playlistUrl } = JSON.parse(
        await getStream(playlistUrlRequest.data)
      );

      const filename = `downloads/${trackInfo.permalink}.mp3`;

      const { exitCode } = await execa("ffmpeg", [
        "-i",
        playlistUrl,
        "-c",
        "copy",
        "-map",
        "a",
        filename,
        "-y"
      ]);

      if (exitCode === 0) {
        console.log("Success! now i need to write the id3 tags... someday");
      }
    } catch (error) {
      console.error(error);
    }
  }
}
