export default class FixedQueue<T> {
  length: number;
  private data: Array<T>;

  constructor(length: number) {
    this.length = length;
    this.data = [];
  }

  push(item: T) {
    this.data.push(item);
    if (this.data.length > this.length) {
      this.data.shift();
    }
  }

  clear() {
    this.data = [];
  }

  toString() {
    return this.data.join("").toString();
  }
}
