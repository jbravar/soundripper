export interface SCStreamDef {
  url: string;
  preset: string;
  duration: number;
  snipped: boolean;
  format: {
    protocol: "hls" | "progressive";
    mime_type: string;
  };
  quality: "sq" | "hq";
}

export interface SCTrackInfo {
  avatar_url: string;
  created_at: string;
  permalink: string;
  permalink_url: string;
  uri: string;
  urn: string;
  url: string;
  username: string;
  full_duration: number;
  media: {
    transcodings: SCStreamDef[];
  };
  title: string;
  duration: number;
  artwork_url: string;
  genre: string;
  label_name: string;
}
