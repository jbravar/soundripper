import FixedQueue from "../util/FixedQueue";

/* eslint-disable no-magic-numbers */
export function captureGroup(
  needle: string,
  haystack: string,
  groupDelimiter = ["[", "]"]
) {
  if (groupDelimiter.length !== 2)
    throw new Error("expected groupDelimiter length of 2");

  const chars = haystack.split("");
  const buff = new FixedQueue(needle.length);

  const found = [];
  let r = 0;
  let e = [];
  while (chars.length) {
    const c = chars.shift();
    if ((r > 0 || buff.toString() === needle) && c === groupDelimiter[0]) r++;
    if (r > 0) e.push(c);
    if (r > 0 && c === groupDelimiter[1]) r--;
    if (r === 0 && e.length) {
      found.push(e.join(""));
      e = [];
    }
    buff.push(c);
  }

  return found.filter(xs => xs.length);
}
/* eslint-enable no-magic-numbers */
