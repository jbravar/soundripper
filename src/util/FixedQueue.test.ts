/* eslint-disable no-magic-numbers */
import test from "ava";
import FixedQueue from "./FixedQueue";

test("push", t => {
  const queue = new FixedQueue(4);
  queue.push("a");
  t.is(queue.toString(), "a");
  queue.push("b");
  queue.push("c");
  queue.push("d");
  t.is(queue.toString(), "abcd");
  queue.push("e");
  t.is(queue.toString(), "bcde");
});

test("clear", t => {
  const queue = new FixedQueue(4);
  queue.push("a");
  queue.push("b");
  queue.push("c");
  queue.push("d");
  t.is(queue.toString(), "abcd");
  queue.clear();
  t.is(queue.toString(), "");
});
